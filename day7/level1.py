INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    pos = [int(c) for c in ",".join(f.readlines()).split(",")]

x_min = min(pos)
x_max = max(pos)

min_fuel = 1e9
for p in range(x_min, x_max):
    min_fuel = min(min_fuel, sum([abs(c-p) for c in pos]))

print(min_fuel)
