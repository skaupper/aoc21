import copy
import re
from typing import Dict, List, Optional

INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"
# INPUT_FILE = "example3.txt"

with open(INPUT_FILE) as f:
    lines = [l.strip() for l in f.readlines()]


def flatten(l):
    return [c for r in l for c in r]


def to_int(c):
    return ord(c) - ord("A")


def to_chr(r_idx):
    return chr(r_idx + ord("A"))


ROOM_PATTERN = re.compile(r"\s*#+([A-Z])#([A-Z])#([A-Z])#([A-Z])#+\s*")
# ROOM_PATTERN = re.compile(r"\s*#+([A-Z])#([A-Z])#([A-Z])#+\s*")
# ROOM_PATTERN = re.compile(r"\s*#+([A-Z])#([A-Z])#+\s*")
ROOM_CNT = 4
ROOM_DEPTH = 2


def parse_rooms(lines):
    rooms = [[] for _ in range(ROOM_CNT)]
    for d in range(ROOM_DEPTH):
        m = ROOM_PATTERN.match(lines[2 + d])
        for i in range(ROOM_CNT):
            rooms[i].append(m.group(i + 1))
    return rooms


rooms = parse_rooms(lines)
halls = [None for _ in range(ROOM_CNT + 3)]
COSTS: Dict[str, int] = {chr(i + ord("A")): 10 ** i for i in range(ROOM_CNT)}

# Map hall indices to effective hall indices
HALL_IDX_MAP = {0: 0, 1: 1, 2: 3, 3: 5, 4: 7, 5: 9, 6: 10}
# HALL_IDX_MAP = {0: 0, 1: 1, 2: 3, 3: 5, 4: 6}
ROOM_IDX_MAP = {i: (i + 1) * 2 for i in range(ROOM_CNT)}


def print_map(rooms, halls):
    print("#" * (6 + 2 * ROOM_CNT - 1))
    # Print hallway
    eff_hall = ["#"] + ["."] * (4 + 2 * ROOM_CNT - 1) + ["#"]
    for h_idx, h_ele in enumerate(halls):
        if h_ele is not None:
            eff_hall[HALL_IDX_MAP[h_idx] + 1] = h_ele[0]

    print("".join(eff_hall))

    # Print room line #0
    line = ["#"] * 3
    for r in rooms:
        entry = r[0] if r[0] is not None else "."
        line += [entry, "#"]
    line += ["#"] * 2
    print("".join(line))

    # Print room line #1-#n
    for d_idx in range(1, ROOM_DEPTH):
        line = [" ", " ", "#"]
        for r in rooms:
            entry = r[d_idx] if r[d_idx] is not None else "."
            line += [entry, "#"]
        print("".join(line))

    # Print room bottom
    print("  " + "#" * (ROOM_CNT * 2 + 1))
    print()


def get_hall_indices_between(start, end) -> List[int]:
    hall_indices = []
    for idx, eff in HALL_IDX_MAP.items():
        if start <= eff <= end:
            hall_indices.append(idx)
    return hall_indices


def calc_cost(
    rooms: List[List[Optional[str]]],
    halls: List[Optional[str]],
    room_idx: int,
    depth_idx: int,
    hall_idx: int,
    step_cost: int,
    from_room: bool,
):
    c = 0

    # Can the element move out of the room?
    if any(ele is not None for ele in rooms[room_idx][:depth_idx]):
        return None
    if not from_room and rooms[room_idx][depth_idx] is not None:
        return None
    c += (depth_idx + 1) * step_cost

    eff_hall_idx = HALL_IDX_MAP[hall_idx]
    eff_room_idx = ROOM_IDX_MAP[room_idx]
    start = min(eff_hall_idx, eff_room_idx)
    end = max(eff_hall_idx, eff_room_idx)
    hall_indices = get_hall_indices_between(start, end)

    # Something is blocking in the hallway
    if not from_room:
        hall_indices.remove(hall_idx)
    if any(halls[idx] is not None for idx in hall_indices):
        return None

    c += (end - start) * step_cost
    return c


def is_finished(rooms):
    return all(
        all(r_ele == to_chr(r_idx) for r_ele in r) for r_idx, r in enumerate(rooms)
    )


def estimated_remaining_cost(rooms, halls):
    c = 0

    # Calculate minimum cost for elements to move from their current room to their target room
    for r_idx, r in enumerate(rooms):
        for d_idx, r_ele in enumerate(r):
            if r_ele == to_chr(r_idx) or r_ele is None:
                continue

            start = ROOM_IDX_MAP[r_idx]
            end = ROOM_IDX_MAP[to_int(r_ele)]
            c += (
                len(get_hall_indices_between(min(start, end), max(start, end)))
                - 1
                + 2
                + d_idx
            ) * COSTS[r_ele]

    # Calculate minimum cost for elements in the hallway to move to their target room
    for h_idx, h_ele in enumerate(halls):
        if h_ele is None:
            continue

        start = h_idx
        end = ROOM_IDX_MAP[r_idx]
        c += (
            len(get_hall_indices_between(min(start, end), max(start, end))) - 1 + 1
        ) * COSTS[h_ele]

    return c


def do_sim(rooms, halls, cost, min_cost=-1):
    if min_cost != -1 and cost + estimated_remaining_cost(rooms, halls) >= min_cost:
        return min_cost

    # print_map(rooms, halls)
    assert (
        len(list(filter(lambda e: e is not None, flatten(rooms) + halls)))
        == ROOM_DEPTH * ROOM_CNT
    )

    if is_finished(rooms):
        return cost

    # Try to move all elements of all rooms to all possible spots in the hallway
    for r_idx, r in enumerate(rooms):
        # Check if the room is already finished
        if all(r_ele is None or r_ele == to_chr(r_idx) for r_ele in r):
            continue

        for d_idx, r_ele in enumerate(r):
            if r_ele is None:
                continue
            step_cost = COSTS[r_ele]

            # Element in room may be moved
            for h_idx in range(len(halls)):
                energy = calc_cost(rooms, halls, r_idx, d_idx, h_idx, step_cost, True)
                if energy is None:
                    continue

                # Path is free
                rooms_new = copy.deepcopy(rooms)
                rooms_new[r_idx][d_idx] = None
                halls_new = halls[:]
                halls_new[h_idx] = r_ele
                energy_cost = do_sim(rooms_new, halls_new, cost + energy, min_cost)
                if min_cost == -1 or energy_cost < min_cost:
                    min_cost = energy_cost

    # Try to move all elements on the hallway to their respective rooms
    for h_idx, h_ele in enumerate(halls):
        if h_ele is None:
            continue
        r_idx = to_int(h_ele)
        step_cost = COSTS[h_ele]

        # We can try to move into their room if at least one spot is free,
        # and all contained elements do belong in this room
        depth = None
        for d_idx, r_ele in reversed(list(enumerate(rooms[r_idx]))):
            if r_ele is None:
                depth = d_idx
                break
            if r_ele != h_ele:
                break

        # If no appropriate empty spot is found, move on to the next hallway element
        if depth is None:
            continue

        # Try moving into the room
        energy = calc_cost(rooms, halls, r_idx, depth, h_idx, step_cost, False)
        if energy is None:
            continue

        rooms_new = copy.deepcopy(rooms)
        rooms_new[r_idx][depth] = h_ele
        halls_new = halls[:]
        halls_new[h_idx] = None
        energy_cost = do_sim(rooms_new, halls_new, cost + energy, min_cost)
        if min_cost == -1 or energy_cost < min_cost:
            min_cost = energy_cost

    assert min_cost != -1
    return min_cost


energy = do_sim(rooms, halls, 0, estimated_remaining_cost(rooms, halls) * 3)
print(energy)
