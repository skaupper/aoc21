PARAMS = [
    (10, 2),
    (15, 16),
    (14, 9),
    (15, 0),
    (-8, 1),
    (10, 12),
    (-16, 6),
    (-4, 6),
    (11, 3),
    (-3, 5),
    (12, 9),
    (-7, 3),
    (-15, 2),
    (-7, 3),
]


def calc_max(digits=None, stack=None):
    # Default arguments
    if digits is None:
        digits = []

    if stack is None:
        stack = []

    # Exit condition
    idx = len(digits)
    if idx >= len(PARAMS):
        return digits

    # Parameter for current iteration
    dx, dy = PARAMS[idx]
    if dx < 0:
        assert 0 < len(stack) < 7
        last_idx, last_dy = stack[-1]
        stack = stack[:-1]
    else:
        stack = stack + [(idx, dy)]

    # Check all possible digits, starting with the largest
    for d in reversed(range(1, 10)):
        if dx < 0 and not (digits[last_idx] + last_dy + dx == d):
            continue

        res = calc_max(digits + [d], stack)
        if res is not None:
            return res


print("".join(str(i) for i in calc_max()))
