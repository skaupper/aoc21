INPUT_FILE = "input.txt"

with open(INPUT_FILE, "r") as f:
    diag = [l.strip() for l in f.readlines()]


oxygen = diag[:]
co2 = diag[:]


bit_idx = 0
while len(oxygen) > 1:
    counts = [
        list(map(lambda e: (e, l.count(e)), set(l))) for l in list(zip(*oxygen))[::-1]
    ]
    counts = [list(sorted(l, key=lambda e: int(e[0]))) for l in reversed(counts)]
    counts = [list(map(lambda e: e[1], l)) for l in counts]
    mc = "1" if counts[bit_idx][1] >= counts[bit_idx][0] else "0"
    oxygen = list(filter(lambda e: e[bit_idx] == mc, oxygen))
    bit_idx += 1

bit_idx = 0
while len(co2) > 1:
    counts = [
        list(map(lambda e: (e, l.count(e)), set(l))) for l in list(zip(*co2))[::-1]
    ]
    counts = [list(sorted(l, key=lambda e: int(e[0]))) for l in reversed(counts)]
    counts = [list(map(lambda e: e[1], l)) for l in counts]
    mc = "1" if counts[bit_idx][1] < counts[bit_idx][0] else "0"
    co2 = list(filter(lambda e: e[bit_idx] == mc, co2))
    bit_idx += 1


assert len(co2) == 1
assert len(oxygen) == 1

oxygen = oxygen[0]
co2 = co2[0]

print("0b" + oxygen)
print("0b" + co2)
print(int(oxygen, 2) * int(co2, 2))
