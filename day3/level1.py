INPUT_FILE = "input.txt"

with open(INPUT_FILE, "r") as f:
    diag = [l.strip() for l in f.readlines()]

diag_trans = list(zip(*diag))[::-1]

bin_length = len(diag_trans)
bin_str = reversed([max(set(l), key=l.count) for l in diag_trans])

gamma = int("".join(bin_str), 2)
epsilon = gamma ^ ((1 << bin_length) - 1)

print(gamma * epsilon)
