from typing import Counter


INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]

template = lines[0]

rules = {}
for l in lines[2:]:
    key, _, val = l.split(" ")
    rules[(key[0], key[1])] = val


pair_counter = Counter()
for i, c in enumerate(template):
    if i >= len(template) - 1:
        continue
    pair_counter.update({(c, template[i + 1]): 1})


def do_step(counter, rules):
    new_counter = Counter()
    for r in rules:
        incr = counter[r]
        new_pair_1 = (r[0], rules[r])
        new_pair_2 = (rules[r], r[1])
        new_counter.update({r: -incr})
        new_counter.update({new_pair_1: incr})
        new_counter.update({new_pair_2: incr})
    counter.update(new_counter)


STEPS = 40
for i in range(STEPS):
    do_step(pair_counter, rules)


ele_counter = Counter()
for pair in pair_counter:
    ele_counter.update({pair[0]: pair_counter[pair]})
ele_counter.update(template[-1])

min_ele = ele_counter.most_common()[-1]
max_ele = ele_counter.most_common()[0]

print(min_ele)
print(max_ele)
print(max_ele[1] - min_ele[1])
