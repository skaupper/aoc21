INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]

template = lines[0]

rules = {}
for l in lines[2:]:
    key, _, val = l.split(" ")
    rules[(key[0], key[1])] = val


def do_step(template, rules):
    res = []
    for i, c in enumerate(template):
        res.append(c)
        if i < len(template) - 1:
            key = (c, template[i + 1])
            if key in rules:
                res.append(rules[key])
    return res


STEPS = 10
# print("".join(template))
for i in range(STEPS):
    template = do_step(template, rules)
    # print("".join(template))
    # print()


counts = [(c, template.count(c)) for c in set(template)]
min_ele = min(counts, key=lambda e: e[1])
max_ele = max(counts, key=lambda e: e[1])

print(min_ele)
print(max_ele)
print(max_ele[1] - min_ele[1])
