import re

INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"


def sign(n):
    if n > 0:
        return 1
    elif n < 0:
        return -1
    else:
        return 0


class Vector2D:
    VECTOR_PATTERN = re.compile(r"(\d+),(\d+) -> (\d+),(\d+)")

    def __init__(self, x1, y1, x2, y2):
        self.p1 = (x1, y1)
        self.p2 = (x2, y2)
        self.dir = (sign(x2 - x1), sign(y2 - y1))

    def __repr__(self):
        return f"Vector2D{{{self.p1=}; {self.p2=}; {self.dir=}}}"

    @classmethod
    def from_str(cls, str):
        m = cls.VECTOR_PATTERN.match(str)
        assert m
        return cls(int(m.group(1)), int(m.group(2)), int(m.group(3)), int(m.group(4)))


with open(INPUT_FILE, "r") as f:
    vectors = [Vector2D.from_str(l.strip()) for l in f.readlines()]


class Map:
    def __init__(self, map_2d):
        self.map = map_2d

    @classmethod
    def from_vectors(cls, vectors):
        max_x = max(map(lambda v: max(v.p1[0], v.p2[0]), vectors))
        max_y = max(map(lambda v: max(v.p1[1], v.p2[1]), vectors))

        map_2d = [[0 for i in range(max_x + 1)] for j in range(max_y + 1)]
        for v in vectors:
            x = v.p1[0]
            y = v.p1[1]
            incr_x = v.dir[0]
            incr_y = v.dir[1]

            while (x, y) != v.p2:
                map_2d[y][x] += 1
                x += incr_x
                y += incr_y

            map_2d[y][x] += 1

        return cls(map_2d)

    def draw(self):
        for row in self.map:
            for c in row:
                print("." if c == 0 else c, end="")
            print()


m = Map.from_vectors(vectors)
m.draw()
print()

more_than_2 = 0
for row in m.map:
    for c in row:
        if c >= 2:
            more_than_2 += 1
print(more_than_2)
