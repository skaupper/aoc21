INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"


with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]

idx = lines.index("")
dots = [tuple(l.split(",")) for l in lines[:idx]]
folds = [tuple(l.rsplit(" ", 1)[-1].split("=")) for l in lines[idx + 1 :]]


def fold(dots, fold):
    fold_dir = fold[0]
    fold_idx = int(fold[1])

    new_dots = []
    for d in dots:
        x = int(d[0])
        y = int(d[1])
        if fold_dir == "x" and x > fold_idx:
            new_x = fold_idx - (x - fold_idx)
            new_dots.append((new_x, y))
        elif fold_dir == "y" and y > fold_idx:
            new_y = fold_idx - (y - fold_idx)
            new_dots.append((x, new_y))
        else:
            new_dots.append((x, y))
    return new_dots


print(sorted(dots))
dots = list(set(fold(dots, folds[0])))
print(sorted(dots))
print(len(dots))
