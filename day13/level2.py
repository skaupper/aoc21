INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"


with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]

idx = lines.index("")
dots = [tuple(l.split(",")) for l in lines[:idx]]
folds = [tuple(l.rsplit(" ", 1)[-1].split("=")) for l in lines[idx + 1 :]]


def fold(dots, fold):
    fold_dir = fold[0]
    fold_idx = int(fold[1])

    new_dots = []
    for d in dots:
        x = int(d[0])
        y = int(d[1])
        if fold_dir == "x" and x > fold_idx:
            new_x = fold_idx - (x - fold_idx)
            new_dots.append((new_x, y))
        elif fold_dir == "y" and y > fold_idx:
            new_y = fold_idx - (y - fold_idx)
            new_dots.append((x, new_y))
        else:
            new_dots.append((x, y))
    return new_dots


for f in folds:
    dots = fold(dots, f)
dots = list(set(dots))

max_x = int(max(dots, key=lambda d: d[0])[0])+1
max_y = int(max(dots, key=lambda d: d[1])[1])+1
print(max_x, max_y)

m = [[" "] * max_x for _ in range(max_y)]

for d in dots:
    x = int(d[0])
    y = int(d[1])
    m[y][x] = "#"

for y in range(max_y):
    for x in range(max_x):
        print(m[y][x], end="")
    print()
