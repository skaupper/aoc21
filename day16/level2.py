INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"
# INPUT_FILE = "example3.txt"
# INPUT_FILE = "example4.txt"

with open(INPUT_FILE, "r") as f:
    bit_stream = bin(int([l.strip() for l in f.readlines()][0], 16))[2:]

bit_stream = "0" * ((4 - (len(bit_stream) % 4)) % 4) + bit_stream


class Operator:
    @classmethod
    def from_bit_stream(cls, version, typ, bit_stream):
        inst = cls()
        inst.version = version
        inst.type = typ

        length_mode = bit_stream[0]
        if length_mode == "0":
            size = bit_stream[1:16]
            bit_stream = bit_stream[16:]
            inst.sub_packets = parse_all_packets(bit_stream[: int(size, 2)])
            bit_stream = bit_stream[int(size, 2) :]
        elif length_mode == "1":
            size = bit_stream[1:12]
            bit_stream = bit_stream[12:]
            inst.sub_packets = []
            for _ in range(int(size, 2)):
                obj, bit_stream = parse_packet(bit_stream)
                inst.sub_packets.append(obj)

        return inst, bit_stream

    def get_version_sum(self):
        return self.version + sum(p.get_version_sum() for p in self.sub_packets)

    def get_value(self):
        if self.type == 0:
            return sum(p.get_value() for p in self.sub_packets)
        elif self.type == 1:
            res = self.sub_packets[0].get_value()
            for p in self.sub_packets[1:]:
                res *= p.get_value()
            return res
        elif self.type == 2:
            return min(p.get_value() for p in self.sub_packets)
        elif self.type == 3:
            return max(p.get_value() for p in self.sub_packets)
        elif self.type == 5:
            assert len(self.sub_packets) == 2
            return (
                1
                if self.sub_packets[0].get_value() > self.sub_packets[1].get_value()
                else 0
            )
        elif self.type == 7:
            assert len(self.sub_packets) == 2
            return (
                1
                if self.sub_packets[0].get_value() == self.sub_packets[1].get_value()
                else 0
            )
        elif self.type == 6:
            assert len(self.sub_packets) == 2
            return (
                1
                if self.sub_packets[0].get_value() < self.sub_packets[1].get_value()
                else 0
            )
        assert False, f"Unknown operator type '{self.type}'"


class Literal:
    @classmethod
    def from_bit_stream(cls, version, typ, bit_stream):
        inst = cls()
        inst.version = version
        inst.type = typ

        value = 0
        prefix = "1"
        while prefix == "1":
            prefix = bit_stream[0]
            value = (value << 4) + int(bit_stream[1:5], 2)
            bit_stream = bit_stream[5:]

        inst.value = value
        return inst, bit_stream

    def get_version_sum(self):
        return self.version

    def get_value(self):
        return self.value


def parse_packet(bit_stream):
    v = int(bit_stream[:3], 2)
    t = int(bit_stream[3:6], 2)
    bit_stream = bit_stream[6:]
    if t == 4:
        obj, bit_stream = Literal.from_bit_stream(v, t, bit_stream)
    else:
        obj, bit_stream = Operator.from_bit_stream(v, t, bit_stream)
    return obj, bit_stream


def parse_all_packets(bit_stream):
    packets = []
    while "1" in bit_stream:
        p, bit_stream = parse_packet(bit_stream)
        packets.append(p)
    return packets


packets = parse_all_packets(bit_stream)
print(packets[0].get_value())
