import sys

sys.setrecursionlimit(100000)


INPUT_FILE = "input.txt"
INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    risk_map = [l.strip() for l in f.readlines()]


h = len(risk_map)
w = len(risk_map[0])

distance_map = [[-1] * w for _ in range(h)]


# print((w, h))


def fill_distance_map(p=(0, 0), risk=0):
    x = p[0]
    y = p[1]

    if x < 0 or x >= w or y < 0 or y >= h:
        return

    if risk >= distance_map[y][x] and distance_map[y][x] != -1:
        return

    distance_map[y][x] = risk
    risk = risk + int(risk_map[y][x])
    fill_distance_map((x + 1, y), risk)
    fill_distance_map((x - 1, y), risk)
    fill_distance_map((x, y + 1), risk)
    fill_distance_map((x, y - 1), risk)


fill_distance_map()

print(distance_map[-1][-1] - distance_map[0][0])

for r in distance_map:
    for c in r:
        print(f"{c:4}", end="")
    print()
