import heapq
import copy


INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    risk_map = [l.strip() for l in f.readlines()]


def extend_columns(line, factor=5):
    l = [int(c) for c in line]
    for i in range(1, factor):
        tmp = [(int(c) + i) for c in line]
        tmp = [c if c <= 9 else c - 9 for c in tmp]
        l += tmp
    return l


def extend_rows(map, factor=5):
    m = copy.deepcopy(map)
    for i in range(1, factor):
        tmp = [[(c + i) for c in r] for r in map]
        tmp = [[c if c <= 9 else c - 9 for c in r] for r in tmp]
        m += tmp
    return m


risk_map = [extend_columns(r) for r in risk_map]
risk_map = extend_rows(risk_map)


h = len(risk_map)
w = len(risk_map[0])

distance_map = [[-1] * w for _ in range(h)]


class PointRisk:
    def __init__(self, x, y, risk=0):
        self.x = x
        self.y = y
        self.risk = risk

    def __lt__(self, other):
        return self.risk < other.risk


to_visit = [PointRisk(0, 0, -int(risk_map[0][0]))]

while len(to_visit) > 0:
    p = heapq.heappop(to_visit)
    if p.x < 0 or p.x >= w or p.y < 0 or p.y >= h:
        continue

    p.risk += int(risk_map[p.y][p.x])
    if distance_map[p.y][p.x] <= p.risk and distance_map[p.y][p.x] != -1:
        continue

    distance_map[p.y][p.x] = p.risk
    heapq.heappush(to_visit, PointRisk(p.x + 1, p.y, p.risk))
    heapq.heappush(to_visit, PointRisk(p.x - 1, p.y, p.risk))
    heapq.heappush(to_visit, PointRisk(p.x, p.y + 1, p.risk))
    heapq.heappush(to_visit, PointRisk(p.x, p.y - 1, p.risk))

print(distance_map[-1][-1])
