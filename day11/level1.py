INPUT_FILE = "input.txt"
#INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]


class Octopus:
    def __init__(self, energy):
        self.energy = energy
        self.has_flashed = False

    def incr(self):
        self.energy += 1

    def will_flash(self):
        return self.energy > 9

    def flash(self):
        if self.will_flash():
            self.has_flashed = True
            return True
        return False

    def reset(self):
        if self.has_flashed:
            self.energy = 0
        self.has_flashed = False


m = [[Octopus(int(c)) for c in l] for l in lines]

STEPS = 100

h = len(m)
w = len(m[0])


def do_incr(x, y):
    if x < 0 or x >= w or y < 0 or y >= h:
        return 0

    octo = m[y][x]
    octo.incr()

    if not octo.will_flash() or octo.has_flashed:
        return 0

    m[y][x].flash()

    cnt = 1
    cnt += do_incr(x + 1, y)
    cnt += do_incr(x - 1, y)
    cnt += do_incr(x, y + 1)
    cnt += do_incr(x, y - 1)
    cnt += do_incr(x + 1, y + 1)
    cnt += do_incr(x + 1, y - 1)
    cnt += do_incr(x - 1, y + 1)
    cnt += do_incr(x - 1, y - 1)
    return cnt


def do_step():
    cnt = 0
    for y, r in enumerate(m):
        for x, _ in enumerate(r):
            cnt += do_incr(x, y)

    for r in m:
        for o in r:
            o.reset()

    return cnt


cnt = 0
for s in range(STEPS):
    cnt += do_step()

print(cnt)
