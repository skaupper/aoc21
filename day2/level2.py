INPUT_FILE = "input.txt"


class Command:
    def __init__(self, cmd_str):
        direction, amt = cmd_str.split(" ", 1)
        self.dir = direction
        self.amount = int(amt)


with open(INPUT_FILE, "r") as f:
    commands = [Command(l.strip()) for l in f.readlines()]


x = 0
d = 0
aim = 0

for c in commands:
    if c.dir == "forward":
        x += c.amount
        d += aim*c.amount
    elif c.dir == "up":
        aim -= c.amount
    elif c.dir == "down":
        aim += c.amount


print(f"{x=}; {d=}")
print(x * d)
