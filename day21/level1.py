# p_fields = [4 - 1, 8 - 1]
p_fields = [3 - 1, 4 - 1]
p_score = [0, 0]


DIE = 0
rolls = 0


def next_dice_value():
    global DIE
    global rolls
    val = DIE + 1
    DIE = (DIE + 1) % 100
    rolls += 1
    return val


while True:
    move = sum(next_dice_value() for _ in range(3))
    p_fields[0] = (p_fields[0] + move) % 10
    p_score[0] += p_fields[0] + 1
    if p_score[0] >= 1000:
        break

    move = sum(next_dice_value() for _ in range(3))
    p_fields[1] = (p_fields[1] + move) % 10
    p_score[1] += p_fields[1] + 1
    if p_score[1] >= 1000:
        break

print(p_score)
print(rolls)
print(min(p_score) * rolls)
