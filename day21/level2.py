import itertools

WINNING_VALUE = 21


def add_tuples(t1, t2):
    return (t1[0] + t2[0], t1[1] + t2[1])


cache = {}


def do_roll(turn, fields, scores):
    key = (turn, fields, scores)
    if key in cache:
        return cache[key]

    if scores[0] >= WINNING_VALUE:
        res = (1, 0)
        cache[key] = res
        return res
    elif scores[1] >= WINNING_VALUE:
        res = (0, 1)
        cache[key] = res
        return res

    res = (0, 0)
    rolls = (1, 2, 3)

    for r in itertools.product(rolls, rolls, rolls):
        steps = sum(r)
        f = list(fields)
        s = list(scores)
        f[turn] = (f[turn] + steps) % 10
        s[turn] += f[turn] + 1
        res = add_tuples(res, do_roll((turn + 1) % 2, tuple(f), tuple(s)))

    cache[key] = res
    return res


# STARTING_POS = (4 - 1, 8 - 1)
STARTING_POS = (3 - 1, 4 - 1)

wins = do_roll(0, STARTING_POS, (0, 0))
print(max(wins))
