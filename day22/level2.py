from dataclasses import dataclass
import re

INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"
# INPUT_FILE = "example3.txt"
# INPUT_FILE = "example4.txt"
# INPUT_FILE = "example5.txt"

with open(INPUT_FILE) as f:
    lines = [l.strip() for l in f.readlines()]


def flatten(l):
    return [c for r in l for c in r]


def sign(n):
    if n == 0:
        return 0
    if n < 0:
        return -1
    return 1


NEXT_ID = 0


def get_next_id():
    global NEXT_ID
    NEXT_ID += 1
    return NEXT_ID - 1


@dataclass(frozen=True)
class Cube:
    x: int
    y: int
    z: int

    width: int
    height: int
    depth: int

    state: bool
    id: int

    PATTERN = re.compile(
        r"(on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)"
    )

    @classmethod
    def from_str(cls, l):
        m = cls.PATTERN.match(l)
        state = m.group(1) == "on"
        x = int(m.group(2))
        width = int(m.group(3)) - x + 1
        y = int(m.group(4))
        height = int(m.group(5)) - y + 1
        z = int(m.group(6))
        depth = int(m.group(7)) - z + 1
        return cls(x, y, z, width, height, depth, state, get_next_id())

    def is_valid(self):
        return (self.width > 0) and (self.height > 0) and (self.depth > 0)

    def get_volume(self):
        assert self.is_valid()
        return self.width * self.height * self.depth

    def do_cancel(self, other):
        self_data = (self.x, self.y, self.z, self.width, self.height, self.depth)
        other_data = (other.x, other.y, other.z, other.width, other.height, other.depth)
        return self_data == other_data

    def _do_intersect_one_way(self, other):
        x_between = (self.x <= other.x <= (self.x + self.width - 1)) or (
            self.x <= (other.x + other.width - 1) <= (self.x + self.width - 1)
        )
        y_between = (self.y <= other.y <= self.y + self.height - 1) or (
            self.y <= (other.y + other.height - 1) <= (self.y + self.height - 1)
        )
        z_between = (self.z <= other.z <= self.z + self.depth - 1) or (
            self.z <= other.z + other.depth - 1 <= self.z + self.depth - 1
        )

        # One corner lies within the second cube
        if x_between and y_between and z_between:
            return True

        # Intersection along the X axis
        if y_between and z_between:
            p1 = self.x - other.x
            p2 = self.x - (other.x + other.width - 1)
            if sign(p1) != sign(p2):
                return True
            p1 = other.x - (self.x + self.width - 1)
            p2 = (other.x + other.width - 1) - (self.x + self.width - 1)
            if sign(p1) != sign(p2):
                return True

        # Intersection along the Y axis
        if x_between and z_between:
            p1 = other.y - self.y
            p2 = (other.y + other.height - 1) - self.y
            if sign(p1) != sign(p2):
                return True
            p1 = other.y - (self.y + self.height - 1)
            p2 = (other.y + other.height - 1) - (self.y + self.height - 1)
            if sign(p1) != sign(p2):
                return True

        # Intersection along the Z axis
        if x_between and y_between:
            p1 = other.z - self.z
            p2 = (other.z + other.depth - 1) - self.z
            if sign(p1) != sign(p2):
                return True
            p1 = other.z - (self.z + self.depth - 1)
            p2 = (other.z + other.depth - 1) - (self.z + self.depth - 1)
            if sign(p1) != sign(p2):
                return True

        return False

    def do_intersect(self, other):
        return self._do_intersect_one_way(other) or other._do_intersect_one_way(self)

    def overlap(self, other):
        if not self.do_intersect(other):
            return [self]

        sub_cubes = []

        # Back
        sub_cubes.append(
            Cube(
                self.x,
                self.y,
                self.z,
                self.width,
                self.height,
                (other.z - self.z),
                self.state,
                self.id,
            )
        )
        # Front
        sub_cubes.append(
            Cube(
                self.x,
                self.y,
                (other.z + other.depth),
                self.width,
                self.height,
                (self.z + self.depth) - (other.z + other.depth),
                self.state,
                self.id,
            )
        )
        # Top
        z = max(other.z, self.z)
        end_z = min(other.z + other.depth, self.z + self.depth)
        depth = end_z - z
        sub_cubes.append(
            Cube(
                self.x,
                self.y,
                z,
                self.width,
                (other.y - self.y),
                depth,
                self.state,
                self.id,
            )
        )
        # Bottom
        sub_cubes.append(
            Cube(
                self.x,
                (other.y + other.height),
                z,
                self.width,
                (self.y + self.height) - (other.y + other.height),
                depth,
                self.state,
                self.id,
            )
        )
        # Left
        y = max(other.y, self.y)
        end_y = min(other.y + other.height, self.y + self.height)
        height = end_y - y
        sub_cubes.append(
            Cube(self.x, y, z, (other.x - self.x), height, depth, self.state, self.id)
        )
        # Right
        sub_cubes.append(
            Cube(
                other.x + other.width,
                y,
                z,
                (self.x + self.width) - (other.x + other.width),
                height,
                depth,
                self.state,
                self.id,
            )
        )

        # Overlap
        x = max(self.x, other.x)
        y = max(self.y, other.y)
        z = max(self.z, other.z)
        end_x = min(self.x + self.width, other.x + other.width)
        end_y = min(self.y + self.height, other.y + other.height)
        end_z = min(self.z + self.depth, other.z + other.depth)
        sub_cubes.append(
            Cube(x, y, z, end_x - x, end_y - y, end_z - z, self.state, self.id)
        )

        non_empty_sub_cubes = [c for c in sub_cubes if c.is_valid()]
        return non_empty_sub_cubes


cubes = [Cube.from_str(l) for l in lines if len(l) > 0 and not l.startswith("#")]
orig_cubes = cubes[:]
cubes_map = {c.id: [c] for c in cubes}


# Check all cubes for overlaps with all other cubes
# If a cube does not overlap with any other, move it to the final 'non_overlapping_cubes' list
non_overlapping_cubes = []

while len(cubes_map) > 0:
    i, c = next(iter(cubes_map.items()))
    assert len(c) > 0

    # If an overlap is detected, this list contains the disassembled cube
    overlapped_cubes = []
    overlapped = False

    # Compare the selected cubes to another cube (resp. its subcubes)
    for j, c2 in cubes_map.items():
        if i == j:
            continue
        assert j > i

        new_c2 = []

        for k, c3 in enumerate(c2):
            if not c[0].do_intersect(c3) or c[0].do_cancel(c3) or overlapped:
                new_c2.append(c3)
                continue

            # If an overlap is detected, disassemble both cubes
            overlapped = True
            overlapped_cubes = c[0].overlap(c3)
            overlap2 = c3.overlap(c[0])
            new_c2 += overlap2 + c2[k + 1 :]
            break

        # If an overlap is detected, update the list of cubes
        if overlapped:
            cubes_map[j] = new_c2
            break

    # If no overlap was detected at all, avoid doing comparisions with this cube again
    if not overlapped:
        non_overlapping_cubes.append(c[0])

    # Update the list of relevant cubes and delete the entry if the list becomes empty
    cubes_map[i] = overlapped_cubes + c[1:]
    if len(cubes_map[i]) == 0:
        print(f"Cube #{i} done")
        del cubes_map[i]

cubes = sorted(non_overlapping_cubes, key=lambda c: (c.id, c.x, c.y, c.z))

# Remove cancelled cubes
reduced_cubes = []
for i, c in enumerate(reversed(cubes)):
    if not any(c.do_cancel(c2) for c2 in reduced_cubes):
        reduced_cubes.append(c)

cubes = filter(lambda c: c.state, reduced_cubes)

# Count switched on cubes
switched_on = sum(c.get_volume() for c in cubes)
print(switched_on)
