INPUT_FILE = "input.txt"
INPUT_FILE = "example.txt"
#INPUT_FILE = "example2.txt"
#INPUT_FILE = "example3.txt"


class Cave:
    def __init__(self, name):
        self.name = name
        self.neighbours = set()

    def is_big(self):
        return self.name == self.name.upper()

    def add_neighbour(self, neighbour):
        self.neighbours.add(neighbour)


all_caves = {}

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]


for l in lines:
    n1, n2 = l.split("-", 1)
    if n1 not in all_caves:
        all_caves[n1] = Cave(n1)
    if n2 not in all_caves:
        all_caves[n2] = Cave(n2)

    all_caves[n1].add_neighbour(n2)
    all_caves[n2].add_neighbour(n1)

print(all_caves)


def find_all_paths(node, path=None):
    if node == "end":
        print(path)
        return 1

    if path is None:
        path = []

    cnt = 0
    for n in all_caves[node].neighbours:
        if n in path and not all_caves[n].is_big():
            continue
        cnt += find_all_paths(n, path + [n])
    return cnt


print(find_all_paths("start", ["start"]))
