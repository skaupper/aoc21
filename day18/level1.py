from dataclasses import dataclass
from typing import Union
from math import ceil

INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"
# INPUT_FILE = "example3.txt"
# INPUT_FILE = "example4.txt"

with open(INPUT_FILE, "r") as f:
    lines = [eval(l.strip()) for l in f.readlines()]


def to_number(nr, parent=None):
    if isinstance(nr, list):
        return SnailNumber.from_list(nr, parent)
    else:
        return RegularNumber(parent, nr)


def next_reg_number_left(nr):
    # Find next left path
    last_curr_nr = nr
    curr_nr = nr.parent
    while curr_nr.left is last_curr_nr:
        if curr_nr.parent is None:
            return None
        last_curr_nr = curr_nr
        curr_nr = curr_nr.parent

    left = curr_nr.left

    # Find rightmost regular number
    reg_nr = left
    while not isinstance(reg_nr, RegularNumber):
        reg_nr = reg_nr.right

    return reg_nr


def next_reg_number_right(nr):
    # Find next right path
    last_curr_nr = nr
    curr_nr = nr.parent
    while curr_nr.right is last_curr_nr:
        if curr_nr.parent is None:
            return None
        last_curr_nr = curr_nr
        curr_nr = curr_nr.parent

    right = curr_nr.right

    # Find leftmost regular number
    reg_nr = right
    while not isinstance(reg_nr, RegularNumber):
        reg_nr = reg_nr.left

    return reg_nr


def replace_nr_with_value(nr, value):
    reg_nr = RegularNumber(nr.parent, value)
    if nr.parent.left == nr:
        nr.parent.left = reg_nr
    elif nr.parent.right == nr:
        nr.parent.right = reg_nr
    else:
        assert False


@dataclass
class RegularNumber:
    parent: "SnailNumber"
    value: int

    def __repr__(self):
        return self.dump()

    def get_magnitude(self):
        return self.value

    def do_explode(self, _):
        return False

    def dump(self):
        return f"{self.value}"

    def do_split(self):
        if self.value < 10:
            return False

        l_nr = RegularNumber(None, self.value // 2)
        r_nr = RegularNumber(None, int(ceil(self.value / 2)))
        new_nr = SnailNumber(self.parent, l_nr, r_nr)
        l_nr.parent = new_nr
        r_nr.parent = new_nr
        if self.parent.left is self:
            self.parent.left = new_nr
        elif self.parent.right is self:
            self.parent.right = new_nr
        return True


@dataclass
class SnailNumber:
    parent: "SnailNumber"
    left: Union["SnailNumber", RegularNumber]
    right: Union["SnailNumber", RegularNumber]

    @classmethod
    def from_list(cls, l, parent=None):
        assert isinstance(l, list) and len(l) == 2
        nr = cls(parent, to_number(l[0]), to_number(l[1]))
        nr.left.parent = nr
        nr.right.parent = nr
        return nr

    def __repr__(self):
        return self.dump()

    def dump(self):
        return f"[{self.left.dump()}, {self.right.dump()}]"

    def get_magnitude(self):
        return 3 * self.left.get_magnitude() + 2 * self.right.get_magnitude()

    def do_explode(self, depth=0):
        if depth < 4:
            if self.left.do_explode(depth + 1):
                return True
            if self.right.do_explode(depth + 1):
                return True
            return False

        reg_nr = next_reg_number_left(self)
        if reg_nr is not None:
            reg_nr.value += self.left.value

        reg_nr = next_reg_number_right(self)
        if reg_nr is not None:
            reg_nr.value += self.right.value

        replace_nr_with_value(self, 0)
        return True

    def do_split(self):
        if self.left.do_split():
            return True
        return self.right.do_split()

    def reduce(self):
        while True:
            if self.do_explode():
                continue
            if not self.do_split():
                break

    def add(self, nr):
        new_nr = SnailNumber(None, self, nr)
        self.parent = new_nr
        nr.parent = new_nr
        new_nr.reduce()
        return new_nr


numbers = [SnailNumber.from_list(l) for l in lines]
total_nr = numbers[0]
for nr in numbers[1:]:
    total_nr = total_nr.add(nr)
print(total_nr.dump())
print(total_nr.get_magnitude())
