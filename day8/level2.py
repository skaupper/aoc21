INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"

with open(INPUT_FILE, "r") as f:
    digit_lines = [
        [d.strip().split(" ") for d in l.split("|", 1)] for l in f.readlines()
    ]


class Display:
    """
     aaa
    b   c
    b   c
     ddd
    e   f
    e   f
     ggg



     1 Segment: -
     2 Segments: 1
     3 Segments: 7
     4 Segments: 4
     5 Segments: 2, 3, 5
     6 Segments: 6, 9, 0
     7 Segments: 8

     Order of training:
     1: c, f (not in order)
     7: a (fixed)
     4: b, d (not in order)
    """

    DIGIT_MAPPINGS = {
        1: ["c", "f"],
        2: ["a", "c", "d", "e", "g"],
        3: ["a", "c", "d", "f", "g"],
        4: ["b", "c", "d", "f"],
        5: ["a", "b", "d", "f", "g"],
        6: ["a", "b", "d", "e", "f", "g"],
        7: ["a", "c", "f"],
        8: ["a", "b", "c", "d", "e", "f", "g"],
        9: ["a", "b", "c", "d", "f", "g"],
        0: ["a", "b", "c", "e", "f", "g"],
    }

    def _decode(self, segs):
        segs = sorted(list(set(segs)))
        for d, m in self.DIGIT_MAPPINGS.items():
            if m == segs:
                return d
        raise ValueError("could not decode " + "".join(segs))

    def __init__(self):
        self.mapping = {}
        self.digit_mapping = {i: None for i in range(10)}
        for i in range(7):
            self.mapping[chr(ord("a") + i)] = map(lambda i: chr(i + ord("a")), range(7))

    def train(self, train_digits):
        d = {i + 1: filter(lambda s: len(s) == i + 1, train_digits) for i in range(7)}
        segments = list(map(lambda i: chr(i + ord("a")), range(7)))

        # Number 1
        for s in segments:
            if s in d[2][0]:
                self.mapping[s] = ["c", "f"]
            else:
                self.mapping[s].remove("c")
                self.mapping[s].remove("f")
        self.digit_mapping[1] = d[2][0]

        # Number 7
        for s in segments:
            if s in d[3][0]:
                if s not in d[2][0]:
                    self.mapping[s] = ["a"]
            else:
                self.mapping[s].remove("a")
        self.digit_mapping[7] = d[3][0]

        # Number 4
        for s in segments:
            if s in d[4][0]:
                if s not in d[2][0]:
                    self.mapping[s] = ["b", "d"]
            else:
                try:
                    self.mapping[s].remove("b")
                    self.mapping[s].remove("d")
                except ValueError:
                    pass
        self.digit_mapping[4] = d[4][0]

        # Number 8
        self.digit_mapping[8] = d[7][0]

        # 5 Segments
        hor_segments = []
        for digit in d[5]:
            for s in digit:
                if all([s in digit2 for digit2 in d[5]]):
                    hor_segments.append(s)
        hor_segments = list(
            set(filter(lambda s: len(self.mapping[s]) > 1, hor_segments))
        )

        for s in segments:
            if s in hor_segments:
                self.mapping[s] = ["d", "g"]
            else:
                try:
                    self.mapping[s].remove("d")
                    self.mapping[s].remove("g")
                except ValueError:
                    pass

        # 6 Segments
        occ = {s: 0 for s in segments}
        for digit in d[6]:
            for s in digit:
                occ[s] += 1
        cde = list(filter(lambda s: occ[s] != 3, occ))

        for s in cde:
            if "c" in self.mapping[s]:
                self.mapping[s] = ["c"]
            elif "d" in self.mapping[s]:
                self.mapping[s] = ["d"]
            elif "e" in self.mapping[s]:
                self.mapping[s] = ["e"]

        to_remove = ["c", "d", "e"]
        for s in segments:
            if len(self.mapping[s]) > 1:
                for r in to_remove:
                    try:
                        self.mapping[s].remove(r)
                    except ValueError:
                        pass

        # Remove arrays
        for s in segments:
            assert len(self.mapping[s]) == 1
            self.mapping[s] = self.mapping[s][0]

        # Reverse mapping
        self.rev_mapping = {}
        for k, v in self.mapping.items():
            self.rev_mapping[v] = k

    def deduce(self, digit):
        digit = map(lambda d: self.mapping[d], digit)
        return self._decode(digit)

    @staticmethod
    def is_easy(s):
        easy_lengths = [2, 3, 4, 7]
        return len(s) in easy_lengths


values = []
for train_digits, digits in digit_lines:
    disp = Display()
    disp.train(train_digits)
    v = 0
    for d in digits:
        v = v * 10 + disp.deduce(d)
    values.append(v)
print(sum(values))
