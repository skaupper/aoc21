INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"

with open(INPUT_FILE, "r") as f:
    digit_lines = [
        [d.strip().split(" ") for d in l.split("|", 1)] for l in f.readlines()
    ]


class Display:
    """
     aaa
    b   c
    b   c
     ddd
    e   f
    e   f
     ggg



     1 Segment: -
     2 Segments: 1
     3 Segments: 7
     4 Segments: 4
     5 Segments: 2, 3, 5
     6 Segments: 6, 9, 0
     7 Segments: 8

     Order of training:
     1: c, f (not in order)
     7: a (fixed)
     4: b, d (not in order)
    """

    def __init__(self):
        self.mapping = {}
        for i in range(7):
            self.mapping[chr(ord("a") + i)] = map(lambda i: chr(i + ord("a")), range(7))

    def train(self, train_digits):
        d = {i + 1: filter(lambda s: len(s) == i + 1, train_digits) for i in range(7)}

        # Number 1
        for s in d[2][0]:
            self.mapping[s] = ["c", "f"]

        # Number 7
        for s in d[3][0]:
            if s not in d[2][0]:
                self.mapping[s] = "a"

        # Number 4
        for s in d[4][0]:
            if s not in d[2][0]:
                self.mapping[s] = ["b", "d"]

        print(self.mapping)

    @staticmethod
    def is_easy(s):
        easy_lengths = [2, 3, 4, 7]
        return len(s) in easy_lengths


ez = 0
for train_digits, digits in digit_lines:
    print(digits)
    ez += sum(map(lambda x: 1 if x else 0, [Display.is_easy(d) for d in digits]))
print(ez)
