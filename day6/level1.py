INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    state = [int(c) for c in ",".join(f.readlines()).split(",")]


TOTAL_DAYS = 80
FIRST_SPAWN_TIME = 9
SPAWN_TIME = 7


def calc_descendants(days_until_spawn, total_rem_days):
    if days_until_spawn >= total_rem_days:
        return 0
    rem_days = total_rem_days - (days_until_spawn + 1)
    return (
        calc_descendants(SPAWN_TIME - 1, rem_days)
        + calc_descendants(FIRST_SPAWN_TIME - 1, rem_days)
        + 1
    )


spawn_map = {d: calc_descendants(d, TOTAL_DAYS) for d in range(FIRST_SPAWN_TIME)}

print(sum(map(lambda d: spawn_map[d], state)) + len(state))
