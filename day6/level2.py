INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    state = [int(c) for c in ",".join(f.readlines()).split(",")]


TOTAL_DAYS = 256
FIRST_SPAWN_TIME = 9
SPAWN_TIME = 7


cache = {}


def calc_descendants(days_until_spawn, total_rem_days):
    cache_key = (days_until_spawn, total_rem_days)
    if cache_key in cache:
        return cache[cache_key]

    if days_until_spawn >= total_rem_days:
        cache[cache_key] = 0
        return 0
    rem_days = total_rem_days - (days_until_spawn + 1)

    descendants = (
        calc_descendants(SPAWN_TIME - 1, rem_days)
        + calc_descendants(FIRST_SPAWN_TIME - 1, rem_days)
        + 1
    )
    cache[cache_key] = descendants
    return descendants


spawn_map = {d: calc_descendants(d, TOTAL_DAYS) for d in range(FIRST_SPAWN_TIME)}

print(sum(map(lambda d: spawn_map[d], state)) + len(state))
