INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"


class BingoBoard:
    def __init__(self, size, lines):
        self.size = size
        self.lines = [
            [(int(n), False) for n in filter(len, l.strip().split(" "))]
            for l in lines[:size]
        ]

    def mark_number(self, number):
        for i, l in enumerate(self.lines):
            for j, c in enumerate(l):
                if c[0] == number:
                    self.lines[i][j] = (number, True)

    def is_bingo(self):
        # Check rows
        for l in self.lines:
            bingo = True
            for c in l:
                if not c[1]:
                    bingo = False
                    break
            if bingo:
                return True

        # Check columns
        for x in range(self.size):
            bingo = True
            for y in range(self.size):
                if not self.lines[y][x][1]:
                    bingo = False
                    break
            if bingo:
                return True

        return False

    def sum_unmarked(self):
        s = 0
        for l in self.lines:
            for c in l:
                if not c[1]:
                    s += c[0]
        return s


with open(INPUT_FILE, "r") as f:
    lines = f.readlines()


#
# Step 0: Parse inputs
#
from math import ceil

inputs = [int(n) for n in lines[0].split(",")]

lines = lines[2:]

SIZE = 5
boards = [
    BingoBoard(SIZE, lines[(SIZE + 1) * i : (SIZE + 1) * (i + 1)])
    for i in range(int(ceil(len(lines) / 6)))
]


#
# Step 1: Mark numbers until done
#
not_won = boards
for n in inputs:
    for b in boards:
        b.mark_number(n)
        if len(not_won) == 1 and b in not_won and b.is_bingo():
            print(n)
            print(b.sum_unmarked())
            print(b.sum_unmarked() * n)
            break

    not_won = list(filter(lambda b: not b.is_bingo(), boards))
    if len(not_won) == 0:
        break
