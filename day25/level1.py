INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]

m = {(x, y): c for y, r in enumerate(lines) for x, c in enumerate(r) if c != "."}
w = max(map(lambda pos: pos[0], m)) + 1
h = max(map(lambda pos: pos[1], m)) + 1

EAST = ">"
SOUTH = "v"


def print_map(map, width, height):
    for y in range(height):
        for x in range(width):
            print(map[(x, y)] if (x, y) in map else ".", end="")
        print()
    print()


def do_step(map, width, height):
    east = filter(lambda c: m[c] == EAST, m)
    south = filter(lambda c: m[c] == SOUTH, m)
    new_map = {}

    for x, y in east:
        n_x = (x + 1) % width
        if (n_x, y) not in map:
            new_map[(n_x, y)] = EAST
        else:
            new_map[(x, y)] = EAST

    for x, y in south:
        n_y = (y + 1) % height
        can_move = (x, n_y) not in new_map
        if (x, n_y) in map and map[(x, n_y)] == SOUTH:
            can_move = False
        if can_move:
            new_map[(x, n_y)] = SOUTH
        else:
            new_map[(x, y)] = SOUTH

    return new_map


# print_map(m, w, h)
# input()

step = 0
last_map = None
while m != last_map:
    last_map = m
    m = do_step(m, w, h)
    step += 1

    # print(step)
    # print_map(m, w, h)
    # input()

print(step)
