import itertools


INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    line = [l.strip() for l in f.readlines()][0]

_, x_range, y_range = line.split("=", 2)
x_min, x_max = [int(n) for n in x_range.split(",")[0].split("..", 1)]
y_min, y_max = [int(n) for n in y_range.split("..", 1)]

# print((x_min, x_max))
# print((y_min, y_max))


max_steps = 2 * (abs(y_min) - 1) + 1

# print(max_steps)


#
# Calculate all possible velocities for X
#


def get_x_distance(vel):
    return vel * (vel + 1) / 2


def is_in_x_range(vel, x_min, x_max):
    if get_x_distance(vel) < x_min:
        return False, None

    # if x_min <= get_x_distance(vel) <= x_max:
    #     return True, (vel, None)

    d = 0
    start = None
    stop = None
    for step, delta in enumerate(reversed(range(1, vel + 1))):
        d += delta
        if x_min <= d <= x_max:
            if start is None:
                start = step + 1
        elif start is not None and stop is None:
            stop = step

    if start is not None:
        return True, (start, stop)

    return False, None


def get_all_possible_x_velocities(x_min, x_max):
    vels = []
    # Hits in 1 Step:
    for v in range(x_min, x_max + 1):
        vels.append((v, (1, 1)))

    # Possible multi-step solultions
    for v in range(1, x_min):
        in_range, step_range = is_in_x_range(v, x_min, x_max)
        if not in_range:
            continue

        vels.append((v, step_range))
    return vels


#
# Calculate all possible velocities for Y
#


def is_in_y_range(vel, y_min, y_max):
    d = 0
    start = None
    stop = None

    start_vel = abs(vel)
    if vel >= 0:
        start_vel += 1

    for step, delta in enumerate(range(start_vel, abs(y_min) + 1)):
        d += delta
        if y_min <= -d <= y_max:
            if start is None:
                start = step + 1
        elif start is not None and stop is None:
            stop = step
            break
    if stop is None:
        stop = start

    if vel > 0:
        if start is not None:
            start += 2 * vel + 1
        if stop is not None:
            stop += 2 * vel + 1

    if start is not None:
        assert stop is not None
        return True, (start, stop)

    return False, None


def get_all_possible_y_velocities(y_min, y_max):
    vels = []
    # Possible solultions
    for v in range(y_min, abs(y_min)):
        in_range, step_range = is_in_y_range(v, y_min, y_max)
        if not in_range:
            continue

        vels.append((v, step_range))
    return vels


#
#
#


def intersect_ranges(e):
    x_tup, y_tup = e
    x_range = x_tup[1]
    y_range = y_tup[1]

    if x_range[1] is None:
        if y_range[1] >= x_range[0]:
            x_range = (x_range[0], y_range[1])
        else:
            return False

    return (y_range[0] <= x_range[0] <= y_range[1]) or (
        y_range[0] <= x_range[1] <= y_range[1]
    )


x_velocities = set(get_all_possible_x_velocities(x_min, x_max))
y_velocities = set(get_all_possible_y_velocities(y_min, y_max))


# print(sorted(v[0] for v in x_velocities))
# print(sorted(v[0] for v in y_velocities))


all_possible_pairs = itertools.product(x_velocities, y_velocities)

true_pairs = list(
    map(lambda e: (e[0][0], e[1][0]), filter(intersect_ranges, all_possible_pairs))
)
print(len(set(true_pairs)))
print(sorted(true_pairs))
# print(max(true_pairs, key=lambda e: e[1]))
