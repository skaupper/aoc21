INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]


def parse_chunk(s):
    CHUNK_DELIM = {"(": ")", "[": "]", "{": "}", "<": ">"}

    while len(s) > 0:
        opener = s[0]
        if opener not in CHUNK_DELIM:
            return s, None, False

        s, err, found = parse_chunk(s[1:])
        if err is not None:
            return s, err, False

        if len(s) == 0:
            return s, None, True

        closer = s[0]
        if closer != CHUNK_DELIM[opener]:
            return s, closer, False

        s = s[1:]

    return s, None, False


ERROR_SCORES = {")": 3, "]": 57, "}": 1197, ">": 25137}

errors = [parse_chunk(s) for s in lines]
errors = list(map(lambda e: e[1], filter(lambda e: e[1] is not None, errors)))
print(errors)
error_score = sum(map(lambda e: ERROR_SCORES[e], errors))
print(error_score)
