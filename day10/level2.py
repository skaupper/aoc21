INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"

with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]


def parse_chunk(s, closing_seq):
    CHUNK_DELIM = {"(": ")", "[": "]", "{": "}", "<": ">"}

    while len(s) > 0:
        opener = s[0]
        if opener not in CHUNK_DELIM:
            return s, None, closing_seq

        s, err, closing_seq = parse_chunk(s[1:], closing_seq)
        if err is not None:
            return s, err, closing_seq

        if len(s) == 0:
            closing_seq.append(CHUNK_DELIM[opener])
            return s, None, closing_seq

        closer = s[0]
        if closer != CHUNK_DELIM[opener]:
            return s, closer, closing_seq

        s = s[1:]

    return s, None, closing_seq


CLOSING_SCORES = {")": 1, "]": 2, "}": 3, ">": 4}

closing_sequence = [parse_chunk(s, []) for s in lines]
closing_sequence = list(
    map(lambda e: e[2], filter(lambda e: e[1] is None, closing_sequence))
)
print(closing_sequence)


def calc_closing_score(seq):
    score = 0
    for c in seq:
        score = score * 5 + CLOSING_SCORES[c]
    return score


closing_score = list(sorted(map(calc_closing_score, closing_sequence)))
print(closing_score[len(closing_sequence) // 2])
