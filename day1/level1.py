INPUT_FILE = "input.txt"

with open(INPUT_FILE, "r") as f:
    depths = [int(l.strip()) for l in f.readlines()]

# Fancy solution
# print(sum(map(lambda e: 1 if e[1] > e[0] else 0, zip(depths, depths[1:]))))

# Boring solution
incr = 0
for i in range(1, len(depths)):
    if depths[i] > depths[i - 1]:
        incr += 1

print(incr)
