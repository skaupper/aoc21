INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    depth_map = [[int(c) for c in l.strip()] for l in f.readlines()]


h = len(depth_map)
w = len(depth_map[0])

low_points = []
for y in range(h):
    for x in range(w):
        curr_depth = depth_map[y][x]
        if (
            (x < w - 1 and depth_map[y][x + 1] <= curr_depth)
            or (x > 0 and depth_map[y][x - 1] <= curr_depth)
            or (y < h - 1 and depth_map[y + 1][x] <= curr_depth)
            or (y > 0 and depth_map[y - 1][x] <= curr_depth)
        ):
            continue

        low_points.append((x, y))


risk_level = map(lambda p: depth_map[p[1]][p[0]] + 1, low_points)
print(sum(risk_level))
