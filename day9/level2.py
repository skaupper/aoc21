INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE, "r") as f:
    depth_map = [[int(c) for c in l.strip()] for l in f.readlines()]


h = len(depth_map)
w = len(depth_map[0])

low_points = []
for y in range(h):
    for x in range(w):
        curr_depth = depth_map[y][x]
        if (
            (x < w - 1 and depth_map[y][x + 1] <= curr_depth)
            or (x > 0 and depth_map[y][x - 1] <= curr_depth)
            or (y < h - 1 and depth_map[y + 1][x] <= curr_depth)
            or (y > 0 and depth_map[y - 1][x] <= curr_depth)
        ):
            continue

        low_points.append((x, y))


def calc_basin_size(lp):
    basin_map = [[0] * w for _ in range(h)]

    def _recurse_basin_points(x, y):
        if x < 0 or x >= w or y < 0 or y >= h:
            return
        if depth_map[y][x] == 9:
            return
        if basin_map[y][x] == 1:
            return

        basin_map[y][x] = 1
        _recurse_basin_points(x + 1, y)
        _recurse_basin_points(x - 1, y)
        _recurse_basin_points(x, y + 1)
        _recurse_basin_points(x, y - 1)

    _recurse_basin_points(lp[0], lp[1])
    return sum(map(lambda r: sum(r), basin_map))


basins = sorted(map(calc_basin_size, low_points))
print(basins[-3:])

res = 1
for f in basins[-3:]:
    res *= f
print(res)
