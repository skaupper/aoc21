from typing import Dict, Tuple

INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"

with open(INPUT_FILE) as f:
    lines = [l.strip() for l in f.readlines()]


TRANS_TABLE = list(map(lambda e: 1 if e == "#" else 0, lines[0]))
lines = lines[2:]


def read_map(lines):
    m = {}
    for y, r in enumerate(lines):
        for x, c in enumerate(r):
            m[(x, y)] = 1 if c == "#" else 0
    return m


def get_value(map, x, y, default_value):
    if (x, y) not in map:
        return default_value
    return map[(x, y)]


def calc_neighbours(
    map: Dict[Tuple[int, int], int], x: int, y: int, default_value: int
) -> int:
    COORDS = [
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (0, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    ]

    v = 0
    for dx, dy in COORDS:
        key = (x + dx, y + dy)
        curr_v = default_value
        if key in map:
            curr_v = map[key]
        v = v * 2 + curr_v
    return v


def print_map(map, bounds_x, bounds_y):
    for y in range(bounds_y[0] + 1, bounds_y[1]):
        for x in range(bounds_x[0] + 1, bounds_x[1]):
            print("#" if map[(x, y)] == 1 else ".", end="")
        print()
    print()


def do_step(map, bounds_x, bounds_y, default_value):
    new_map = {}
    for y in range(bounds_y[0], bounds_y[1] + 1):
        for x in range(bounds_x[0], bounds_x[1] + 1):
            new_map[(x, y)] = TRANS_TABLE[calc_neighbours(map, x, y, default_value)]

    if default_value == 0:
        new_default_value = TRANS_TABLE[0]
    elif default_value == 1:
        new_default_value = TRANS_TABLE[-1]
    else:
        assert False

    new_bounds_x = (bounds_x[0] - 1, bounds_x[1] + 1)
    new_bounds_y = (bounds_y[0] - 1, bounds_y[1] + 1)
    return new_map, new_bounds_x, new_bounds_y, new_default_value


bounds_y = (0 - 1, len(lines))
bounds_x = (0 - 1, len(lines[0]))
default_value = 0
map = read_map(lines)


STEPS = 50
for i in range(STEPS):
    print(i)
    map, bounds_x, bounds_y, default_value = do_step(
        map, bounds_x, bounds_y, default_value
    )


count_lit = sum(map.values())
print(count_lit)
