import itertools
import numpy as np
from dataclasses import dataclass, field
import re
from typing import Dict, List, Tuple


NEEDED_PAIRS = 12
INPUT_FILE = "input.txt"
# INPUT_FILE = "example.txt"
# INPUT_FILE = "example2.txt"; NEEDED_PAIRS = 3
# INPUT_FILE = "example3.txt"; NEEDED_PAIRS = 3


with open(INPUT_FILE, "r") as f:
    lines = [l.strip() for l in f.readlines()]


@dataclass(frozen=True, order=True)
class Vector3D:
    x: int
    y: int
    z: int

    VECTOR_PATTERN = re.compile(r"(-?\d+),(-?\d+)(,(-?\d+))?")

    @classmethod
    def from_str(cls, str):
        m = cls.VECTOR_PATTERN.match(str)
        x, y = int(m.group(1)), int(m.group(2))
        z_grp = m.group(4)
        z = int(z_grp) if z_grp is not None else 0
        return cls(x, y, z)

    @classmethod
    def from_arr(cls, arr):
        assert len(arr) == 3
        return cls(*arr)

    def manhatten(self):
        return abs(self.x) + abs(self.y) + abs(self.z)

    def neg(self):
        return Vector3D(-self.x, -self.y, -self.z)

    def abs(self):
        return Vector3D(
            abs(self.x),
            abs(self.y),
            abs(self.z),
        )

    def add(self, other):
        return Vector3D(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
        )

    def sub(self, other):
        return Vector3D(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
        )

    ROT_MATRIX_X = np.array(
        [
            [1, 0, 0],  #  1        0       0
            [0, 0, -1],  # 0   cos(a) -sin(a)
            [0, 1, 0],  #  0   sin(a)  cos(a)
        ]
    )

    ROT_MATRIX_Y = np.array(
        [
            [0, 0, 1],  #   cos(a)  0  sin(a)
            [0, 1, 0],  #        0  1       0
            [-1, 0, 0],  # -sin(a)  0  cos(a)
        ]
    )

    ROT_MATRIX_Z = np.array(
        [
            [0, -1, 0],  #  cos(a)  -sin(a)  0
            [1, 0, 0],  #   sin(a)   cos(a)  0
            [0, 0, 1],  #        0        0  1
        ]
    )

    def permutations(self):
        perms = {}
        vec = np.array([self.x, self.y, self.z])
        for rot_x in range(4):
            for rot_y in range(4):
                for rot_z in range(4):
                    curr_perm = Vector3D.from_arr(
                        vec
                        @ (np.linalg.matrix_power(self.ROT_MATRIX_X, rot_x))
                        @ (np.linalg.matrix_power(self.ROT_MATRIX_Y, rot_y))
                        @ (np.linalg.matrix_power(self.ROT_MATRIX_Z, rot_z))
                    )
                    if curr_perm not in perms:
                        perms[curr_perm] = (rot_x, rot_y, rot_z)
        assert len(perms) == 24
        return perms

    def rotate(self, theta):
        if isinstance(theta, list):
            if len(theta) == 0:
                return self
            return self.rotate(theta[0]).rotate(theta[1:])

        return self.__class__(
            *(
                np.array([self.x, self.y, self.z])
                @ np.linalg.matrix_power(self.ROT_MATRIX_X, theta[0])
                @ np.linalg.matrix_power(self.ROT_MATRIX_Y, theta[1])
                @ np.linalg.matrix_power(self.ROT_MATRIX_Z, theta[2])
            )
        )


@dataclass
class Scanner:
    id: int
    beacons: List[Vector3D]
    abs_deltas: Dict[Vector3D, List[Tuple[int, int]]] = field(default_factory=dict)
    abs_deltas_by_distance: Dict[int, List[Vector3D]] = field(default_factory=dict)
    final: bool = False

    transform_pos: List[Vector3D] = field(default_factory=list)
    transform_theta: List[Tuple[int, int, int]] = field(default_factory=list)

    SCANNER_PATTERN = re.compile(r"--- scanner (\d+) ---")

    @classmethod
    def from_str_list(cls, lines):
        if len(lines) == 0:
            return None

        m = cls.SCANNER_PATTERN.match(lines[0])
        id = int(m.group(1))
        beacons = []

        line_idx = 1
        while line_idx < len(lines) and len(lines[line_idx].strip()) > 0:
            beacons.append(Vector3D.from_str(lines[line_idx]))
            line_idx += 1

        inst = cls(id, sorted(beacons))
        inst._calc_deltas()
        return inst

    def add_transform(self, pos, theta, other_scanner=None):
        if not isinstance(theta, list):
            theta = [theta]
        if not isinstance(pos, list):
            pos = [pos]

        if other_scanner is not None:
            pos = other_scanner.transform_pos[:] + pos
            theta = other_scanner.transform_theta[:] + theta

        self.transform_pos = pos
        self.transform_theta = theta

    def get_beacons(self):
        def _transform_beacon(b):
            for i in reversed(range(len(self.transform_pos))):
                b = b.rotate(self.transform_theta[i]).add(self.transform_pos[i])
            return b

        return list(map(_transform_beacon, self.beacons))

    def _calc_deltas(self):
        self.abs_deltas = {}
        self.abs_deltas_by_distance = {}
        for i1, b1 in enumerate(self.get_beacons()):
            for i2, b2 in enumerate(self.get_beacons()):
                if i1 == i2:
                    continue
                d = b1.sub(b2)

                if d not in self.abs_deltas:
                    self.abs_deltas[d] = []
                self.abs_deltas[d].append((i1, i2))

                manhatten = d.manhatten()
                if manhatten not in self.abs_deltas_by_distance:
                    self.abs_deltas_by_distance[manhatten] = []
                self.abs_deltas_by_distance[manhatten].append(d)

    def get_overlaps(self, other):
        overlaps_by_rot = {}
        for manhatten in self.abs_deltas_by_distance:
            if manhatten not in other.abs_deltas_by_distance:
                continue
            deltas1 = self.abs_deltas_by_distance[manhatten]
            deltas2 = other.abs_deltas_by_distance[manhatten]

            for d1 in deltas1:
                for d2 in deltas2:
                    perm, rot_lst = zip(*d2.permutations().items())

                    try:
                        perm_idx = perm.index(d1)
                    except ValueError:
                        continue

                    rot = rot_lst[perm_idx]
                    if rot not in overlaps_by_rot:
                        overlaps_by_rot[rot] = []
                    overlaps_by_rot[rot] += itertools.product(
                        self.abs_deltas[d1], other.abs_deltas[d2]
                    )

        return overlaps_by_rot


def count_common_beacons(s1, s2):
    return len(set(s1.get_beacons()).intersection(s2.get_beacons()))


def calculate_transformation(s1, s2):
    # Get all overlap configurations with more than NEEDED_PAIRS matches (*2 since (0,1) and (1,0) are both reported)
    overlaps = s1.get_overlaps(s2)
    overlaps = list(
        filter(lambda e: len(e[1]) // 2 >= NEEDED_PAIRS * 2, overlaps.items())
    )

    # Just for debugging print all overlaps
    if len(overlaps) == 0:
        return False
    assert len(overlaps) <= 1

    rot, pairs = overlaps[0]

    # Calculate the relative position of scanner s2 to scanner s1
    theta = rot
    rel_pos = None

    p = pairs[0]
    delta_00 = s1.beacons[p[0][0]].sub(s2.beacons[p[1][0]].rotate(theta))
    delta_11 = s1.beacons[p[0][1]].sub(s2.beacons[p[1][1]].rotate(theta))
    delta_01 = s1.beacons[p[0][0]].sub(s2.beacons[p[1][1]].rotate(theta))
    delta_10 = s1.beacons[p[0][1]].sub(s2.beacons[p[1][0]].rotate(theta))

    rel_pos = None
    if delta_00 == delta_11:
        rel_pos = delta_00
    elif delta_01 == delta_10:
        rel_pos = delta_01
    assert rel_pos is not None

    # Apply transformation and do a sanity check
    print(f"{(s1.id, s2.id)} -> {rel_pos}; {theta}")
    s2.add_transform(rel_pos, theta, s1)
    assert count_common_beacons(s1, s2) >= NEEDED_PAIRS
    return True


# Parse input and create a list of scanners
scanners = []
while len(lines) > 0:
    scanners.append(Scanner.from_str_list(lines))
    lines = lines[len(scanners[-1].get_beacons()) + 2 :]


# BFS of intersecting scan regions
finalized = []
to_check = [scanners[0]]
unchecked = scanners[1:]

while len(to_check) > 0:
    s1 = to_check[0]
    to_check = to_check[1:]

    for s2 in unchecked:
        if not calculate_transformation(s1, s2):
            continue

        to_check.append(s2)

    # Update the unchecked list, so all scanners which are up to be checked are excluded
    unchecked = list(
        filter(lambda s: not any(sc.id == s.id for sc in to_check), unchecked)
    )

    # Finalize scanner 1
    s1.final = True
    finalized.append(s1)

scanners = finalized
assert all(s.final for s in scanners)


# Calculate relative scanner positions
def apply_transform(pos, theta):
    s_pos = Vector3D(0, 0, 0)
    for i in reversed(range(len(pos))):
        s_pos = s_pos.rotate(theta[i]).add(pos[i])
    return s_pos


print()
for s in sorted(scanners, key=lambda s: s.id):
    print(f"Scanner pos #{s.id}: {apply_transform(s.transform_pos, s.transform_theta)}")


# Determine set of distinct beacons
beacons = set()
for s in scanners:
    for b in s.get_beacons():
        beacons.add(b)

print(len(beacons))
